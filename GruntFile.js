(function(){

	'use strict';
	const fs = require('fs');

	module.exports = function(grunt) {
		//const fs = require('fs');

		grunt.initConfig({});

		grunt.config.merge({
			target: (function(){
				let targ = '';
				try {
					targ = './scrapers/secrets.json';
					targ = fs.readFileSync(targ,'utf8');
					//grunt.log.write(`HERE: ${targ}`);
					targ = JSON.parse(targ);
					targ = targ.db;
				}
				catch (e) {
					targ = {
						port: 5984,
						hostname: 'lvh.me',
						db:'grants',
						protocol:'http',
					};
				}
				if(!targ.url){
					targ.url = targ.protocol + '://' + targ.hostname + ':' + targ.port + '/' + targ.db + '/';
				}
				return targ;
			})(),
			isProd : (process.env.isProd == 'true')
		});

		grunt.config.merge({
			watch: {
				files: ['couchapp/**/*'],
				tasks: ['build']
			},
			'couch-compile': {
				app: {
					files: {
						'bin/otherviews.json': 'couchapp/otherviews',
					}
				}
			},
			'couch-push': {
				app: {
					options: {
						//user: 'jeff',
						//pass: 'hellojello'
					},
					files: (()=>{
						var f = {};
						//grunt.log.write("HERE:"+grunt.config.get('target').url);
						f[grunt.config.get('target').url] = [
							'bin/otherviews.json',
						];
						return f;
					})()
				}
			},
		});


		grunt.loadNpmTasks('grunt-contrib-watch');
		grunt.loadNpmTasks('grunt-couch');

		grunt.registerTask('default', ['build']);
		grunt.registerTask('build', ['deploy']);
		grunt.registerTask('deploy', [
			'couch-compile',
			'couch-push'
		]);

	};


})();
