![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)



# Datasets of Interest

- [Government of Canada: Proactive Disclosure: Grants and Contributions](https://open.canada.ca/data/en/dataset/432527ab-7aac-45b5-81d6-7597107a7013)
- [Government of Canada: Open Government Inventory](https://open.canada.ca/data/en/dataset/4ed351cf-95d8-4c10-97ac-6b3511f359b7)

