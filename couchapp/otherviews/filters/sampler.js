function(doc, req){
	var form = doc._id;
	form = form.split('.');
	var type = form[0];
	var comp = form[1];

	var numerator = +req.query.numerator;
	var denominator = +req.query.demonminator;

	if(isNaN(numerator)) numerator = 1;
	if(isNaN(denominator)) denominator = 1000;

	if (!Math.imul) Math.imul = function(opA, opB) {
		opB |= 0;
		var result = (opA & 0x003fffff) * opB;
		if (opA & 0xffc00000 /*!== 0*/) result += (opA & 0xffc00000) * opB |0;
		return result |0;
	};

	function cyrb53(str, seed) {
		seed  = seed || 0;
		var h1 = 0xdeadbeef ^ seed;
		var h2 = 0x41c6ce57 ^ seed;
		for (var i = 0, ch; i < str.length; i++) {
			ch = str.charCodeAt(i);
			h1 = Math.imul(h1 ^ ch, 2654435761);
			h2 = Math.imul(h2 ^ ch, 1597334677);
		}
		h1 = Math.imul(h1 ^ h1>>>16, 2246822507) ^ Math.imul(h2 ^ h2>>>13, 3266489909);
		h2 = Math.imul(h2 ^ h2>>>16, 2246822507) ^ Math.imul(h1 ^ h1>>>13, 3266489909);
		return 4294967296 * (2097151 & h2) + (h1>>>0);
	}

	comp = cyrb53(comp);
	comp %= denominator;
	comp = (comp <= numerator);

	return comp;
}
