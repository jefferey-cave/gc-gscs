function (doc) {
	var id = [
    doc.fy,
    doc.quarter,
    doc.recipient_city,
    doc.recipient_legal_name,
    doc.owner_org_en,
  ];
  emit(id, doc.agreement_value);
}
