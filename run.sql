
.bail on
.open 'data\grants.db'
.import '.\data\grant.csv' grants --csv
.open 'data\cleaned.db'

attach 'data\grants.db' as orig;

drop table if exists owner_org;
create table owner_org(
	id text,
	code text,
	chif text,
	name text,
	nom text
);

insert into owner_org(id,code,chif,name,nom)
select distinct 
	owner_org as id,
	trim(substr(owner_org,1,instr(owner_org,'-')-1)) as code, 
	trim(substr(owner_org,instr(owner_org,'-')+1))  as chif,
	trim(substr(owner_org_title,1,instr(owner_org_title,'|')-1)) as name, 
	trim(substr(owner_org_title,instr(owner_org_title,'|')+1)) as nom 
from 
	grants
;

update owner_org 
set    code = chif
where  code = ''
;

drop table if exists projects;
create table projects as (
  "ref_number" TEXT,
  "amendment_number" int,
  "amendment_date" TEXT,
  "agreement_type" TEXT,
  "recipient_type" TEXT,
  "recipient_business_number" TEXT,
  "recipient_legal_name" TEXT,
  "recipient_operating_name" TEXT,
  "research_organization_name" TEXT,
  "recipient_country" TEXT,
  "recipient_province" TEXT,
  "recipient_city" TEXT,
  "recipient_postal_code" TEXT,
  "federal_riding_name_en" TEXT,
  "federal_riding_name_fr" TEXT,
  "federal_riding_number" TEXT,
  "prog_name_en" TEXT,
  "prog_name_fr" TEXT,
  "prog_purpose_en" TEXT,
  "prog_purpose_fr" TEXT,
  "agreement_title_en" TEXT,
  "agreement_title_fr" TEXT,
  "agreement_number" TEXT,
  "agreement_value" TEXT,
  "foreign_currency_type" TEXT,
  "foreign_currency_value" TEXT,
  "agreement_start_date" TEXT,
  "agreement_end_date" TEXT,
  "coverage" TEXT,
  "description_en" TEXT,
  "description_fr" TEXT,
  "naics_identifier" TEXT,
  "expected_results_en" TEXT,
  "expected_results_fr" TEXT,
  "additional_information_en" TEXT,
  "additional_information_fr" TEXT,
  "owner_org" TEXT
);

insert into main.projects
select
	"ref_number" ,
	"amendment_number",
	"amendment_date" ,
	"agreement_type" ,
	"recipient_type" ,
	"recipient_business_number" ,
	"recipient_legal_name" ,
	"recipient_operating_name" ,
	"research_organization_name" ,
	"recipient_country" ,
	"recipient_province" ,
	"recipient_city" ,
	"recipient_postal_code" ,
	"federal_riding_name_en" ,
	"federal_riding_name_fr" ,
	"federal_riding_number" ,
	"prog_name_en" ,
	"prog_name_fr" ,
	"prog_purpose_en" ,
	"prog_purpose_fr" ,
	"agreement_title_en" ,
	"agreement_title_fr" ,
	"agreement_number" ,
	"agreement_value" ,
	"foreign_currency_type" ,
	"foreign_currency_value" ,
	"agreement_start_date" ,
	"agreement_end_date" ,
	"coverage" ,
	"description_en" ,
	"description_fr" ,
	"naics_identifier" ,
	"expected_results_en" ,
	"expected_results_fr" ,
	"additional_information_en" ,
	"additional_information_fr" ,
	"owner_org" 
from orig.grants
;
alter table projects add column id int;
alter table projects add column fy int;
alter table projects add column quarter int;
alter table projects add column dept int;

update projects
set 
	fy = strftime('%Y',agreement_start_date,'start of month','+9 months'),
	quarter = strftime('%m',agreement_start_date,'start of month','+9 months')/4+1
where 
	amendment_number = 0
;
