const fs = require('fs');
const path = require('path');

const fetch = require('node-fetch');
const parser = require('fast-csv').parse;
const lineReader = require('line-reader');

const utils = require('./lib/utils.js');


exports.getGrants = class getGrants{
	constructor(db, opts = {}){
		this._ = {
			opts: {
				url:'https://open.canada.ca/data/dataset/432527ab-7aac-45b5-81d6-7597107a7013/resource/1d15a62f-5656-49ad-8c88-f40ce689d831/download/grants.csv',
				throttle:{
					recs:1357,
					reqs:4,
					timeframe:1000,
				},
				timeout:60000*2,
			}
		};
		this.db = db;
		this.complete = false;
		
		this._.opts = Object.assign(this._.opts,opts);
		if (this._.opts.workdir) {
			this._.opts.workdir = path.resolve(this._.opts.workdir);
			console.log(`Working directory: ${this._.opts.workdir}`);
		}
		else{
			fs.mkdtemp(path.resolve('./data/buffer.grants./'), (err, folder) => {
				this._.opts.workdir = folder;
				console.log(`Working directory: ${this._.opts.workdir}`);
			});
		}
		this.down = 0;
		this.sent = 0;
		this.skip = 0;
		this.buff = 0;
		this.resetkill();
		this.kill += 9999999;
		
		this.sendbuffer = [];
	}

	get url(){
		let path = this._.opts.url;
		return path;
	}
	set url(value){
		this._.opts.url = value;
	}
	
	start(){
		if(this._.timer) return false;
		this._.timer = 1;

		let nextrun = Math.random();
		nextrun *= this._.opts.freqVariance;
		nextrun += this._.opts.frequency;
		nextrun += Date.now();

		this.run().then(()=>{
			nextrun -= Date.now();
			nextrun = Math.max(0,nextrun);
			nextrun = Math.floor(nextrun);

			this._.timer = setTimeout(()=>{
				if(!this._.timer) return;
				this._.timer = null;
				this.start();
			}, nextrun);
		});
	}

	stop(){
		clearTimeout(this._.timer);
		this._.timer = null;
	}

	async run(){
		let wait = 100;
		while(!this._.opts.workdir){
			console.log(`No workdir. Waiting ${wait}ms`);
			await utils.wait(wait);
			wait *= 1.37;
		}

		this.fetch();
		await utils.wait(wait);
		this.process();
		while(!this.isProcessed){
			wait = this.buff * 1;
			wait = Math.min(wait, 55555);
			wait = Math.max(wait, 99);
			wait = Math.floor(wait);
			this.writeProgress();
			console.log(`... ${wait}ms`);
			await utils.wait(wait);
		}
		
		console.log('Run completed');
		try{
			console.log('Cleaning up');
			await Promise.all([
				// DB compaction is not supported on CloudAnt
				//this.db.compact(),
				fs.rmSync(this._.opts.workdir, { recursive: true })
			]);
		}
		catch(e){
			console.log("Clean-up failed");
			console.log(e);
		}
		return null;
	}
	
	fetch(){
		if(this.donefetch) return;		
		this.donefetch = false;
		
		let prog = 0;
		const stream = parser({ headers: true })
			.on('data',  row =>{
				this.down++;
				let file = path.resolve(this._.opts.workdir,`${this.down}.json`.padStart(13,'0'));
				fs.writeFileSync(file, (JSON.stringify(row)+'\n') );
				prog--;
				if(prog <= 0){
					prog = 137;
					this.writeProgress();
				}
			})
			;
		console.log(`Downloading ${this.url}`);
		fetch(this.url).then((resp)=>{
			console.log('Download begun');
			let pipe = resp.body.pipe(stream);
			pipe.on('end',()=>{
				console.log('Download complete.');
				this.writeProgress();
				this.donefetch = true;
			});
			pipe.on('error', err =>{
				console.log('Error during pipe read.');
				console.log(err);
			});
		});
	}
	
	async doproc(){
		let wait = Date.now()+this.waitlen;
		this.isProcessed = false;
		let files = fs
			.readdirSync(this._.opts.workdir)
			;
		this.buff = files.length;
		/*
		 * if(this.kill > Date.now()){
		 * 	this.buff = 0;
		 * 	this.donefetch = true;
		 * }
		 */
		if(this.buff === 0 && this.donefetch){
			this.writeProgress();
			console.log('Uploading complete: ' + files.length);
			this.isProcessed = true;
			return true;
		}
		this.writeProgress();
		
		for(let file of files.slice(0,this.maxitems)){
			file = path.resolve(this._.opts.workdir,file);
			let contents = null;
			try{
				contents = fs.readFileSync(file,'utf8');
				let json = JSON.parse(contents);
				let result = this.send(json);
				fs.unlink(file,err=>{if(err)reject(err);} );
			}
			catch(e){
				console.log(e);
				console.log(contents);
				console.log(file);
			}
		}
		try{
			let writes = await this.flush();
			this.sent += writes.length;
		}
		catch(e){
			console.log(e);
			// Too many requests
			if(e.status === 429){
				this.waitlen = Math.floor(this.waitlen*1.1);
			}
		}
		if(this.buff > 0){
			this.resetkill();
		}
		else if(this.kill <= Date.now()){
			this.writeProgress();
			console.log('\nTIMEOUT: No results found for more than a minute');
			this.isProcessed = true;
			return false;
		}
		
		wait -= Date.now();
		this.procInterval = setTimeout(()=>{this.doproc();},wait);
		this.writeProgress();
		
		return false;
	}
	
	async process(){
		if(this.isProcessing) return this.isProcessing;
		this.waitlen = Math.max(16,Math.ceil(this._.opts.throttle.timeframe / this._.opts.throttle.reqs));
		//let delay = this._.opts.throttle.timeframe;
		this.maxitems = this._.opts.throttle.recs;
		console.log('Upload Begun');
		this.procInterval = setTimeout(()=>{this.doproc();},this.waitlen);
	}
	
	resetkill(){
		this.kill = Date.now() + this._.opts.timeout;
		//console.log('RESET KILL: ' + this.kill);
	}
	
	async send(sub){
		//console.log(sub);
		let rec = sub;
		
		rec.agreement_value = Number.parseFloat(rec.agreement_value);
		rec.amendment_number = Number.parseInt(rec.amendment_number);
		rec.recipient_postal_code = rec.recipient_postal_code.replace(/ /g,'');
		
		let pos = rec.owner_org.split('|');
		rec.owner_org_en = pos[0]; 
		rec.owner_org_fr = pos[1] || pos[0];
		pos = rec.owner_org_title.split('|');
		rec.owner_org_title_en = pos[0];
		rec.owner_org_title_fr = pos[1]; 
		rec.owner_org_id = rec.owner_org.hash();
		
		rec.owner_org = '';
		rec.owner_org_title = '';
		
		let fDate = new Date(rec.agreement_start_date);
		fDate.setMonth(fDate.getMonth()+9);
		rec.fy = fDate.getFullYear();
		rec.quarter = Math.floor(fDate.getMonth()/3)+1;
		
		for(let key in rec){
			if(rec[key].trim){
				rec[key] = rec[key].trim();
			}
			if(rec[key] === '') delete rec[key];
		}
		
		// Massage the hell out of the ID
		let id = rec.ref_number.split('-');
		if(id[0] === 'GC'){
			id.shift();
			id[0] = (Number.parseInt(id[0])+1).toString();
		}
		else if(id[1] === 'CN'){
			id[1] = rec.quarter.toString();
		}
		else{
			rec.owner_org = Number.parseInt(id[0]);
			id.shift();
			id.shift();
		}
		for(let i=0; i<id.length; i++){
			id[i] = id[i].replace(/[^0-9]/g,'');
			id[i] = Number.parseInt(id[i]);
		}
		id.unshift(rec.owner_org_id);
		rec.seq = id[3];
		rec.orig = [id[1],'-Q',id[2]].join('');
		
		// calculate the agreement_number
		if(rec.agreement_number && rec.agreement_number !== ''){
			id = rec.agreement_number;
			id = id.replace(/\s/g,'-');
			for(let len=0; id.length!==len; len=id.length){
				id = id.replace('--','-');
			}
			let a = id.slice(0,id.length/2);
			let b = id.slice(id.length/2+1);
			//console.log(a,'|',b);
			if(a===b){
				id = a;
				rec.agreement_number = rec.agreement_number.slice(0,rec.agreement_number.length/2);
			}
			id = id.replace(/[^A-Za-z0-9\-]/g,'');
			id = [rec.owner_org_id,id];
		}
		else{
			rec.agreement_number = [
				rec.orig,
				(rec.seq||0).toString().padStart(5,'0')
			].join('-');
		}
		
		id.push(rec.amendment_number);
		id = id.join('.');
		rec._id = id;
		//console.log(rec._id);
		
		
		this.sendbuffer.push(rec);
		return rec;
	}
	
	async flush(){
		let buff = this.sendbuffer;
		this.sendbuffer = [];
		let keys = buff.map(r=>r._id);
		let orig = await this.db.allDocs({
			keys: keys,
			include_docs: true,
		});
		orig = orig.rows;
		
		let pushbuff = [];
		for(let i=0; i<orig.length; i++){
			let rec = utils.UniqueRec(orig[i].doc,buff[i]);
			this.skip++;
			if(rec){
				this.skip--;
				pushbuff.push(rec);
			}
		}
		let result = await this.db.bulkDocs(pushbuff);
		return result;
	}


	writeProgress(){
		let down = this.down;
		let buff = this.buff;
		let sent = this.sent;
		let skip = this.skip;
		let size = this.maxitems;
		let wait = this.waitlen;
		process.stdout.write(`\rDown:${down} => Buff:${buff} Sent:${sent} Skip:${skip} (${size}r/${wait}ms) `);
	}
};


