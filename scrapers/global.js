const fs = require('fs');
const PouchDB = require('pouchdb');
const path = require("path");
PouchDB.plugin(require('pouchdb-upsert'));



let settings = process.argv.slice();
settings.shift();
settings.shift();
settings = settings.join(' ').trim();
if(!settings){
	settings = process.cwd();
	settings = `${settings}/scrapers/secrets.json`;
}
settings = path.normalize(settings);
settings = path.resolve(settings);
console.log("Loading: " + settings);
settings = JSON.parse(fs.readFileSync(settings, 'utf8'));
exports.settings = settings;

let opts = {};
if(settings.db.username && settings.db.password){
	opts.auth = {
		username: settings.db.username,
		password: settings.db.password,
	};
}
exports.grants = new PouchDB(settings.db.url,opts);
exports.grants
	.allDocs({limit:1})
	.then(r=>{
		console.log(r);
		if(r){
			console.log('Test Connection successful');
		}
	})
	;