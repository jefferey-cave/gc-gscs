const stringify = require('json-stable-stringify');

exports.stringify = stringify;

exports.UniqueRec = function UniqueRec(a,b){
	a = a || {};
	b = b || {};
	
	a = JSON.parse(JSON.stringify(a));
	b = JSON.parse(JSON.stringify(b));

	let rev = a._rev;
	
	//delete a._id;
	delete a._rev;
	//delete b._id;
	delete b._rev;

	a = stringify(a);
	b = stringify(b);

	let isSame = null;
	if(a !== b){
		isSame = JSON.parse(b);
		if(rev){
			isSame._rev = rev;
		}
	}
	return isSame;
};


exports.isRecUnique = function isUniqueRec(a,b){
	let isSame = UniqueRec(a,b);
	isSame = (isSame !== null);
	return isSame;
};


exports.wait = function wait(time){
	return new Promise((resolve)=>{
		setTimeout(resolve,time);
	});
};


exports.utcParse = function utcParse(date){
	date = date.replace(/[^0-9]/g,'-');
	date = date.split('-');
	date = date.map(d=>{return +d;});
	date[1] = date[1] - 1;
	date = Date.UTC(... date);
	date = new Date(date);
	return date;
};

String.prototype.hash = function() {
	let hash = 0;
	for (let i = 0; i < this.length; i++) {
		let ch = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+ch;
		hash = hash & hash; // Convert to 32bit integer
		hash = Math.abs(hash);
	}
	return hash;
}