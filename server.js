const path = require('path');

const PouchDB = require('pouchdb');
const expressDB = require('express-pouchdb');
const http = require('express')();

let dbdir = path.resolve('./data/db/') + '/';
console.log(dbdir);
let localDB = PouchDB.defaults({
	prefix: dbdir
});

http.use('/', expressDB(localDB));
http.listen(5984);

let sec = localDB('grants');
